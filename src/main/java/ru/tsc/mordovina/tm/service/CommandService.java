package ru.tsc.mordovina.tm.service;

import ru.tsc.mordovina.tm.api.ICommandRepository;
import ru.tsc.mordovina.tm.api.ICommandService;
import ru.tsc.mordovina.tm.model.Command;

public class CommandService implements ICommandService {

    private ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
