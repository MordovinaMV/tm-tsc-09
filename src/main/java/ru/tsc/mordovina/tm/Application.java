package ru.tsc.mordovina.tm;

import ru.tsc.mordovina.tm.api.ICommandController;
import ru.tsc.mordovina.tm.api.ICommandRepository;
import ru.tsc.mordovina.tm.api.ICommandService;
import ru.tsc.mordovina.tm.constant.ArgumentConst;
import ru.tsc.mordovina.tm.constant.TerminalConst;
import ru.tsc.mordovina.tm.controller.CommandController;
import ru.tsc.mordovina.tm.model.Command;
import ru.tsc.mordovina.tm.repository.CommandRepository;
import ru.tsc.mordovina.tm.service.CommandService;
import ru.tsc.mordovina.tm.util.NumberUtil;
import ru.tsc.mordovina.tm.component.Bootstrap;

import java.util.Arrays;
import java.util.Scanner;

public class Application {

    public static void main(final String[] args) {
        final ru.tsc.mordovina.tm.component.Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}
