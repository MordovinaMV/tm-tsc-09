package ru.tsc.mordovina.tm.api;

import ru.tsc.mordovina.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}

