package ru.tsc.mordovina.tm.api;

public interface ICommandController {

    void showErrorArgument();

    void showErrorCommand();

    void showInfo();

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showHelp();

    void exit();

}
