package ru.tsc.mordovina.tm.component;

import ru.tsc.mordovina.tm.api.ICommandController;
import ru.tsc.mordovina.tm.api.ICommandRepository;
import ru.tsc.mordovina.tm.api.ICommandService;
import ru.tsc.mordovina.tm.constant.ArgumentConst;
import ru.tsc.mordovina.tm.constant.TerminalConst;
import ru.tsc.mordovina.tm.controller.CommandController;
import ru.tsc.mordovina.tm.repository.CommandRepository;
import ru.tsc.mordovina.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void start(final String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        parseArgs(args);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    public void parseCommand(final String command) {
        switch (command) {
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.EXIT:
                commandController.exit();
                break;
            default:
                commandController.showErrorCommand();
        }
    }

    public void parseArg(final String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            default:
                commandController.showErrorArgument();
        }
    }

    public void parseArgs(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

}
